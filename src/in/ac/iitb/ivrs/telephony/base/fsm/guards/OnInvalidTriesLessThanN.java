package in.ac.iitb.ivrs.telephony.base.fsm.guards;

import in.ac.iitb.ivrs.telephony.base.IVRSession;

import com.continuent.tungsten.commons.patterns.fsm.Event;
import com.continuent.tungsten.commons.patterns.fsm.Guard;
import com.continuent.tungsten.commons.patterns.fsm.State;

/**
 * Guard condition to check if there have been less than N invalid tries in this session.
 */
public class OnInvalidTriesLessThanN implements Guard<IVRSession, Object> {

	int N;

	/**
	 * Creates the condition.
	 * @param N The number of invalid tries below which this condition should return true.
	 */
	public OnInvalidTriesLessThanN(int N) {
		this.N = N;
	}

	@Override
	public boolean accept(Event<Object> event, IVRSession session, State<?> state) {
		return session.getInvalidTries() < N;
	}

}