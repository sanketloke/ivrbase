package in.ac.iitb.ivrs.telephony.base.fsm;

import in.ac.iitb.ivrs.telephony.base.IVRSession;
import in.ac.iitb.ivrs.telephony.base.events.DisconnectEvent;
import in.ac.iitb.ivrs.telephony.base.events.GotDTMFEvent;
import in.ac.iitb.ivrs.telephony.base.events.NewCallEvent;
import in.ac.iitb.ivrs.telephony.base.events.NullEvent;
import in.ac.iitb.ivrs.telephony.base.events.RecordEvent;
import in.ac.iitb.ivrs.telephony.base.fsm.guards.OnGotDTMFKey;

import com.continuent.tungsten.commons.patterns.fsm.EventTypeGuard;

/**
 * Contains KooKoo events represented as guard conditions for the state machine.
 */
public class EventGuard {

	/**
	 * Proceed to the next state with no specific conditions required.
	 */
	public static EventTypeGuard<IVRSession> proceed;

	/**
	 * Go to the next state if the event is NewCall.
	 */
	public static EventTypeGuard<IVRSession> onNewCall;
	/**
	 * Go to the next state if the event is Record.
	 */
	public static EventTypeGuard<IVRSession> onRecord;
	/**
	 * Go to the next state if the event is GotDTMF.
	 */
	public static EventTypeGuard<IVRSession> onGotDTMF;
	/**
	 * Go to the next state if the event is Hangup or Disconnect.
	 */
	public static EventTypeGuard<IVRSession> onDisconnect;

	/**
	 * Go to the next state if the event is GotDTMF and the key pressed is the array index.
	 */
	public static EventTypeGuard<IVRSession>[] onGotDTMFKey;

	static {
		proceed = new EventTypeGuard<IVRSession>(NullEvent.class);

		onNewCall = new EventTypeGuard<IVRSession>(NewCallEvent.class);
		onRecord = new EventTypeGuard<IVRSession>(RecordEvent.class);
		onGotDTMF = new EventTypeGuard<IVRSession>(GotDTMFEvent.class);
		onDisconnect = new EventTypeGuard<IVRSession>(DisconnectEvent.class);

		onGotDTMFKey = new OnGotDTMFKey[10];
		for (int i = 0; i < 10; ++i)
			onGotDTMFKey[i] = new OnGotDTMFKey(new String[] {"" + i});
	}

}
